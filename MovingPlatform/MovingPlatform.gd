class_name MovingPlatform
extends KinematicBody2D

export var move := false
export var move_when_player_is_on := false
export var speed := 100.0
export var path2d_path: NodePath
export var follow_path: NodePath
export var interpolate := false
export var interpolate_duration := 1.0

onready var path2d := get_node(path2d_path) as Path2D
onready var follow := get_node(follow_path) as PathFollow2D
onready var curve := path2d.curve

var velocity := 0.0

onready var tween := $Tween
onready var tween_sound := $TweenSound
onready var audio_player := $AudioStreamPlayer2D

func _physics_process(delta: float) -> void:
	if interpolate:
		if move:
			if not tween.is_active():
				if not audio_player.playing:
					audio_player.play()
				tween.interpolate_property(audio_player, "volume_db", 0.0, -25, interpolate_duration, Tween.TRANS_SINE, Tween.EASE_IN)
				tween.interpolate_property(follow, "unit_offset", follow.unit_offset, 1.0, interpolate_duration, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
				tween.start()
				yield(tween, "tween_all_completed")
				move = false
				audio_player.stop()
	else:
		if move:
			if follow.unit_offset < 1:
				if not audio_player.playing:
					audio_player.play()
				follow.offset += speed * delta
			else:
				audio_player.stop()
		elif follow.offset > 0:
			if not audio_player.playing:
				audio_player.play()
			follow.offset -= speed * delta
		else:
			audio_player.stop()

func open() -> void:
	move = true
	audio_player.play()

func close() -> void:
	move = false
	audio_player.stop()

func _on_Area2D_body_entered(body: Node) -> void:
	if move_when_player_is_on:
		if body is Player:
			move = true
