class_name Player
extends KinematicBody2D

signal big_heart_taken
signal heart_gained
signal heart_lost
signal received_fall_damage
# warning-ignore:unused_signal
signal trigger_camera

export var health_points := 100
export var speed: float
export var jump_speed: float
export var gravity := 100
export var max_gravity := 1000.0
export var accel := 0.03
export var deaccel := 0.03
export var air_accel := 0.03
export var air_deaccel := 0.03
export var gravity_accel := 0.03
export(float, 0.0, 1.0) var fall_damage_cutoff := 1.0
export(float, 0.0, 20.0) var lean = 4.0
export(float) var lean_accel := 0.03
export(float) var lean_deaccel := 0.1
export var big_hearts := 3


onready var heart_container_scene := preload("res://Heart/Heart.tscn")
onready var default_health_points = health_points
onready var sprite := $Node2D/Sprite as Sprite

onready var death_sound := $DeathSound
onready var moving_sound := $MovingSound
onready var jumping_sound := $JumpingSound

onready var moving_particles := $Node2D/Sprite/MovingParticles
onready var jumping_particles := $Node2D/Sprite/JumpingParticles
onready var death_particles := $Node2D/DeathParticles

onready var animation_player := $AnimationPlayer

export var hearts := 3

export var hearts_instances = [
]

var move_direction: float
var velocity: Vector2
var is_jumping := false
var reached_fall_damage := false
var previous_velocity := Vector2.ZERO
var hit_the_ground := true

var ending := false
export var is_dying := false

func _ready() -> void:
	sprite.material.set("shader_param/dissolve_state", 1.0)

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("reload_checkpoint"):
		State.load_game()
	if not is_dying:
		if event.is_action_pressed("recall_1"):
			if hearts_instances.size() >= 1:
				var heart = hearts_instances[0]
				if is_instance_valid(heart) && not heart.recalling:
					recall(heart)
		if event.is_action_pressed("recall_2"):
			if hearts_instances.size() >= 2:
				var heart = hearts_instances[1]
				if is_instance_valid(heart) && not heart.recalling:
					recall(heart)
		if event.is_action_pressed("recall_3"):
			if hearts_instances.size() >= 3:
				var heart = hearts_instances[2]
				if is_instance_valid(heart) && not heart.recalling:
					recall(heart)

func _physics_process(delta: float) -> void:
	if not is_dying:
		if not ending:
			move_direction = (
				-int(Input.is_action_pressed("move_left")) + 
				int(Input.is_action_pressed("move_right"))
			)
		if move_direction:
			if is_on_floor():
				moving_particles.emitting = true
				if not moving_sound.playing:
					moving_sound.play()
			else:
				moving_particles.emitting = false
				moving_sound.stop()
			sprite.rotation_degrees = lerp(sprite.rotation_degrees, lean * move_direction, lean_accel)
		else:
			moving_particles.emitting = false
			sprite.rotation_degrees = lerp(sprite.rotation_degrees, lean * move_direction, lean_deaccel)
		if is_on_floor():
			is_jumping = false
		if not is_on_floor():
			hit_the_ground = false
			is_jumping = true
			sprite.scale.y = range_lerp(abs(velocity.y), 0, abs(jump_speed), 0.75, 1.25)
			sprite.scale.x = range_lerp(abs(velocity.y), 0, abs(jump_speed), 1.25, 1.0)
		
		if not hit_the_ground and is_on_floor():
			hit_the_ground = true
			sprite.scale.x = range_lerp(abs(previous_velocity.y), 0, abs(1700), 1.5, 3.0)
			sprite.scale.y = range_lerp(abs(previous_velocity.y), 0, abs(1700), 0.8, 0.5)
		
		sprite.scale.x = clamp(lerp(sprite.scale.x, 1, 1 - pow(0.01, delta)), 0.7, 3.0)
		sprite.scale.y = clamp(lerp(sprite.scale.y, 1, 1 - pow(0.01, delta)), 0.7, 3.0)
		
		if not is_jumping && Input.is_action_just_pressed("jump"):
			animation_player.play("Jumping")
			is_jumping = true
			velocity.y = -jump_speed
		
		if reached_fall_damage && is_on_floor():
			do_fall_damage()
			reached_fall_damage = false
		
		reached_fall_damage = reached_fall_damage_velocity()
	else:
		move_direction = 0
		
	if is_jumping:
		if move_direction:
			velocity.x = lerp(velocity.x, move_direction * speed, air_accel)
		else:
			velocity.x = lerp(velocity.x, move_direction * speed, air_deaccel)
	if not is_jumping:
		if move_direction:
			velocity.x = lerp(velocity.x, move_direction * speed, accel)
		else:
			velocity.x = lerp(velocity.x, move_direction * speed, deaccel)
	velocity.y = lerp(velocity.y, max_gravity, gravity_accel)
	velocity = move_and_slide(velocity, Vector2.UP, false)


func reached_fall_damage_velocity() -> bool:
	var fall_damage_velocity = fall_damage_cutoff * max_gravity
	if fall_damage_velocity <= velocity.y:
		return true
	return false

func do_fall_damage() -> void:
	emit_signal("received_fall_damage")
# warning-ignore:return_value_discarded
	spawn_heart()

func take_heart(heart: Heart) -> void:
	heart.animation_player.play("consume")
	yield(heart.animation_player, "animation_finished")
	hearts_instances.pop_at(hearts_instances.find(heart))
	heart.queue_free()
	hearts += 1
	emit_signal("heart_gained")

func take_big_heart(big_heart: BigHeart) -> void:
	big_heart.queue_free()
	big_hearts += 1
	emit_signal("big_heart_taken")

func _on_PickupArea_take_heart(heart) -> void:
	if is_instance_valid(heart):
		heart.queue_free()
		hearts += 1
		emit_signal("heart_gained")

func _on_Player_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event.is_action_pressed("click"):
# warning-ignore:return_value_discarded
		spawn_heart()

func spawn_heart() -> Heart:
	if hearts > 0:
		hearts -= 1
		var heart_container_instance = heart_container_scene.instance()
		heart_container_instance.global_position = self.global_position
		heart_container_instance.id = hearts
		hearts_instances.push_front(heart_container_instance)
		owner.add_child(heart_container_instance)
		emit_signal("heart_lost")
		return heart_container_instance
	return null

func death() -> void:
	if animation_player.current_animation == "Death":
		return
	animation_player.play("Death")
	yield(animation_player, "animation_finished")
	Transition.transition_into()
	yield(Transition, "transitioned_into")
	get_tree().change_scene("res://Game.tscn")

func restart() -> void:
	get_tree().change_scene("res://Game.tscn")

func damage() -> void:
	death()

func has_hearts() -> bool:
	if hearts > 0:
		return true
	return false

func recall(heart) -> void:
		while(true):
			heart.recalling = true
			heart.global_position = lerp(heart.global_position, self.global_position, 0.1)
			var distance = global_position.distance_to(heart.global_position)
			if distance <= 10.0:
				take_heart(heart)
				return
			yield(get_tree(), "idle_frame")

# warning-ignore:function_conflicts_variable
func ending() -> void:
	ending = true
