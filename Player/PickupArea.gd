class_name PickupArea
extends Area2D

signal take_heart(heart)

var picking_up := false
var item_nearby

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("pick_up"):
		picking_up = true
		if item_nearby:
			emit_signal("take_heart", item_nearby)

func _on_PickupArea_body_entered(body: Node) -> void:
	var heart = body as Heart
	if heart:
		item_nearby = heart
	if body is BigHeart:
		pass


func _on_PickupArea_body_exited(body: Node) -> void:
	var heart = body as Heart
	if heart:
		item_nearby = null

