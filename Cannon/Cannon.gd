extends KinematicBody2D

onready var tween := $Tween
onready var opened_position := $OpenedPosition
onready var mover := $RigidBody2D/Mover

var opened := false
var opened_cooldown := true
var has_heart := false
var entities: Array
export var entities_predefined: Array
export var force := Vector2(0, -500)

onready var original_position: Vector2 = mover.global_position

onready var audio_player := $AudioStreamPlayer2D

onready var wind_particles := $WindParticles

func _ready() -> void:
	wind_particles.emitting = false
	for entity_path in entities_predefined:
		var entity = get_node(entity_path)
		entities.push_back(entity)

func _physics_process(delta: float) -> void:
	if opened:
		for entity in entities:
			if entity is RigidBody2D:
				var velocity = force.rotated(rotation)
				if not tween.is_active():
					tween.interpolate_property(entity, "linear_velocity", entity.linear_velocity, velocity, 0.3, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
					tween.start()
			if entity is Player:
				var velocity = force.rotated(rotation) * 2.0
				if not tween.is_active():
					tween.interpolate_property(entity, "velocity", entity.velocity, velocity, 0.3, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
					tween.start()
func open() -> void:
	wind_particles.emitting = true
	audio_player.play()
	opened = true
	tween.start()

func close() -> void:
	audio_player.stop()
	wind_particles.emitting = false
	opened = false


func _on_CannonPickup_body_entered(body: Node) -> void:
	for entity in entities:
		if entity == body:
			return
	if  (body is Heart) || (body is Player):
		entities.push_back(body)

func _on_CannonPickup_body_exited(body: Node) -> void:
	var entity = entities.find(body)
	if entity != -1:
		entities.remove(entity)
