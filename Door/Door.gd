class_name Door
extends StaticBody2D

export var move_speed := 0.01
export var door_node_path: NodePath
onready var door_path := get_node(door_node_path) as PathFollow2D

var opened := false

onready var audio_open := $AudioOpen
onready var audio_close := $AudioClose


func _physics_process(_delta: float) -> void:
	if opened:
		if door_path:
			door_path.unit_offset = lerp(door_path.unit_offset, 1.0, 0.1)

	else:
		if door_path:
			door_path.unit_offset = lerp(door_path.unit_offset, 0.0, 0.1)

func open() -> void:
	opened = true
	audio_open.play()

func close() -> void:
	opened = false
	audio_close.play()
