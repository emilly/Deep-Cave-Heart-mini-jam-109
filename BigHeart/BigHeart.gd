tool
class_name BigHeart
extends Sprite

export var outline := false
export var min_distance := 50

onready var animation_player := $AnimationPlayer as AnimationPlayer
onready var timer := $Timer as Timer

var player

func _ready():
	if not outline:
		$Light2D.enabled = true
		$AnimationPlayer.play("beat")
	else:
		$Light2D.enabled = false


func _physics_process(delta: float) -> void:
	if player:
		position = lerp(position, player.position, 0.1)
		var distance = player.position.distance_to(position)
		if distance <= min_distance:
			if timer.is_stopped():
				timer.connect("timeout", self, "_on_timer_timeout", [player])
				timer.start()
		else:
			timer.stop()

func _on_Area2D_body_entered(body: Node) -> void:
	if body.is_in_group("Player"):
		player = body

func _on_timer_timeout(body) -> void:
	animation_player.play("consume")
	yield(animation_player, "animation_finished")
	body.take_big_heart(self)
