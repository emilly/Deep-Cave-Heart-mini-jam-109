extends AudioStreamPlayer

onready var tween := $Tween

var onetime := true
var triggered := false

func _on_MusicStartTrigger_body_entered(body: Node) -> void:
	if onetime && triggered:
		return
	if body is Player:
		triggered = true
		tween.interpolate_property(self, "volume_db", volume_db, -10, 20.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		tween.start()
		play()
