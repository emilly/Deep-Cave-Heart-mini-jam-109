extends Control

onready var a_button := $a_button
onready var s_button := $s_button
onready var d_button := $d_button
onready var w_button := $w_button

onready var animation_player = $AnimationPlayer as AnimationPlayer

onready var tween_w := $TweenW as Tween
onready var tween_a := $TweenA as Tween
onready var tween_s := $TweenS as Tween
onready var tween_d := $TweenD as Tween

func _physics_process(_delta: float) -> void:
	if Input.is_action_just_pressed("move_left"):
		do_tween(tween_a, a_button)
	if Input.is_action_just_released("move_left"):
		do_tween(tween_a, a_button, true)
	if Input.is_action_just_pressed("move_right"):
		do_tween(tween_d, d_button)
	if Input.is_action_just_released("move_right"):
		do_tween(tween_d, d_button, true)
	if Input.is_action_just_pressed("jump"):
		do_tween(tween_w, w_button)
	if Input.is_action_just_released("jump"):
		do_tween(tween_w, w_button, true)
	if Input.is_action_just_pressed("move_down"):
		do_tween(tween_s, s_button)
	if Input.is_action_just_released("move_down"):
		do_tween(tween_s, s_button, true)

func do_tween(tween: Tween, button: TextureRect, backwards := false) -> void:
	if backwards:
# warning-ignore-all:return_value_discarded
		tween.stop_all()
		tween.interpolate_property(button, "modulate:a", button.modulate.a, 0.2, 0.2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
		tween.interpolate_property(button, "rect_scale", button.rect_scale, Vector2(1, 1), 0.2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
		tween.start()
	else:
		tween.stop_all()
		tween.interpolate_property(button, "modulate:a", button.modulate.a, 1.0, 0.2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
		tween.interpolate_property(button, "rect_scale", button.rect_scale, Vector2(1.25, 1.25), 0.2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
		tween.start()
