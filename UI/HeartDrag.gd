extends Control

onready var animation_player := $AnimationPlayer as AnimationPlayer

func _ready() -> void:
	animation_player.play("remind")
