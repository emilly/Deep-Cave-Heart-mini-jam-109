extends CanvasLayer

var player

onready var hearts := [
	get_node("MarginContainer/Control/Heart1"),
	get_node("MarginContainer/Control/Heart2"),
	get_node("MarginContainer/Control/Heart3")
]

onready var heart_tweens := [
	get_node("Tween1"),
	get_node("Tween2"),
	get_node("Tween3")
]

func _ready() -> void:
	var players := get_tree().get_nodes_in_group("Player")
	if players:
		player = players[0]
		if player is Player:
			player.connect("heart_gained", self, "_on_heart_gained")
			player.connect("heart_lost", self, "_on_heart_lost")

func _on_heart_gained() -> void:
	var heart_texture = hearts[player.hearts - 1]
	var heart_tween = heart_tweens[player.hearts - 1]
	show_heart(heart_texture, heart_tween)

func _on_heart_lost() -> void:
	var heart_texture = hearts[player.hearts]
	var heart_tween = heart_tweens[player.hearts]
	hide_heart(heart_texture, heart_tween)

func show_heart(heart: TextureRect, tween: Tween) -> void:
# warning-ignore-all:return_value_discarded
	tween.stop_all()
	tween.interpolate_property(heart, "modulate:a", heart.modulate.a, 1.0, 0.3, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()
	
func hide_heart(heart: TextureRect, tween: Tween) -> void:
	tween.stop_all()
	tween.interpolate_property(heart, "modulate:a", heart.modulate.a, 0.0, 0.3, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.start()
