extends Control

onready var label := $HBoxContainer/Label as Label

onready var animation_player := $AnimationPlayer as AnimationPlayer

var player

func _ready() -> void:
	var players := get_tree().get_nodes_in_group("Player")
	if players:
		player = players[0]
		player.connect("big_heart_taken", self, "_on_big_heart_taken")

func _on_big_heart_taken() -> void:
	animation_player.play("take")
