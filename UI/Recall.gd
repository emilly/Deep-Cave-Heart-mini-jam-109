extends Control

onready var button_1 := $button_1 as TextureRect
onready var button_2 := $button_2 as TextureRect
onready var button_3 := $button_3 as TextureRect

onready var tween_1 := $tween_1 as Tween
onready var tween_2 := $tween_2 as Tween
onready var tween_3 := $tween_3 as Tween

func _physics_process(_delta: float) -> void:
	if Input.is_action_just_pressed("recall_1"):
		do_tween(button_1, tween_1)
	if Input.is_action_just_pressed("recall_2"):
		do_tween(button_2, tween_2)
	if Input.is_action_just_pressed("recall_3"):
		do_tween(button_3, tween_3)
	

func do_tween(button: TextureRect, tween: Tween) -> void:
# warning-ignore-all:return_value_discarded
	tween.stop_all()
	tween.interpolate_property(button, "modulate:a", button.modulate.a, 1.0, 0.2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(button, "rect_scale", button.rect_scale, Vector2(1.25, 1.25), 0.2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	tween.interpolate_property(button, "modulate:a", button.modulate.a, 0.2, 0.2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(button, "rect_scale", button.rect_scale, Vector2(1, 1), 0.2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
