Made for Mini Jam 109: Depth(s)

This is the version that was [released](https://jamker.itch.io/deep-cave-heart) when submission period ended.

![alt text](https://img.itch.zone/aW1nLzkzMTUyNzgucG5n/315x250%23c/ky5NNV.png)

# How to build
1. Install Godot 3.4.4
2. Import the project using Godot
3. Create a folder named "export" inside the project, then create "windows", "linux", "mac_os" "html5" folders inside the "export" folder.
4. Project -> Export -> Export all -> Release
5. Builds should be in the folder named "export"

# F.A.Q
Q. Do i have to credit you if i use your code?

A. Not required, code is in public domain.

Q. Can i use art/music/fonts from the game?

A. You can check license on music/sound effect in include/attributions.txt, art is CC0.
