class_name Trigger
extends Area2D

export var player_triggered := true
export var one_shot := true

export var trigger_signal := "trigger"
export var trigger_parameters := {}
export var trigger_target: NodePath

onready var target := get_node(trigger_target)

var triggered := false

func _on_Trigger_body_entered(body: Node) -> void:
	if one_shot && triggered:
		return
	if not target:
		return
	if player_triggered:
		if body is Player:
				triggered = true
				target.emit_signal(trigger_signal, trigger_parameters)
	else:
		triggered = true
		target.emit_signal(trigger_signal, trigger_parameters)


func _on_Trigger_body_exited(body: Node) -> void:
	if player_triggered:
		if body is Player:
			pass
