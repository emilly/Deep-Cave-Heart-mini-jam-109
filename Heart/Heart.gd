class_name Heart
extends RigidBody2D

signal holding

export var shatter_velocity := Vector2(0, 0)
export var max_distance := 150

onready var animation_player := $AnimationPlayer as AnimationPlayer
onready var ray := $RayCast2D
onready var line := $RayCast2D/Line2D

var held := false
var health_points := 100
var id := -1
var recalling := false

var last_mouse_position := Vector2(0, 0)
var previous_mouse_position := Vector2(0, 0)

var player

func _ready() -> void:
	var players = get_tree().get_nodes_in_group("Player")
	if players:
		player = players[0]
	$AnimationPlayer.play("beat")
	set_process_input(true)

func _physics_process(_delta: float) -> void:
	if held:
		global_transform.origin = lerp(global_transform.origin, get_global_mouse_position(), 0.1)
	if player:
		ray.rotation = -rotation
		var distance = player.position.distance_to(position)
		ray.cast_to = (player.position - position)
		line.points[1] = ray.cast_to
		line.width = clamp(range_lerp(distance, 0.0, max_distance, 5.0, 0.0), 0.0, 5.0)
		line.default_color.a = clamp(range_lerp(distance, 0.0, max_distance, 1.0, 0.0), 0.0, 1.0)
		if distance > max_distance:
			drop((-self.global_position + get_global_mouse_position()) * 10)
		else:
			var collider = ray.get_collider()
			if collider is Node2D:
				if not (collider is TileMap):
					return
				line.width = 0.0
				drop((-self.global_position + get_global_mouse_position()) * 10)

func pickup() -> void:
	if held:
		return
	mode = RigidBody2D.MODE_STATIC
	held = true
	emit_signal("holding")

func drop(impulse := Vector2.ZERO):
	if held:
		mode = RigidBody2D.MODE_RIGID
		apply_central_impulse(impulse)
		held = false

func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and not event.pressed:
			drop((-self.global_position + get_global_mouse_position()) * 10)

func _on_Heart_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event is InputEventMouseMotion:
		last_mouse_position = event.global_position
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			pickup()

func damage() -> void:
	health_points -= 10
	if health_points <= 0:
		queue_free()

#func recall(player) -> void:
#	recalling = true
#	while(recalling):
#		global_position = lerp(global_position, player.global_position, 0.1)
#		yield(get_tree(), "idle_frame")
#
#func _on_PickupArea_area_entered(area: Area2D) -> void:
#	var area_parent = area.get_parent()
#	if recalling:
#		if area_parent:
#			if area_parent.is_in_group("Player"):
#				area_parent.take_heart(self)
