extends Control

func _on_Start_button_down() -> void:
	Transition.transition_into()
	yield(Transition, "transitioned_into")
	get_tree().change_scene("res://Game.tscn")


func _on_Exit_button_down() -> void:
	get_tree().call_deferred("quit")
