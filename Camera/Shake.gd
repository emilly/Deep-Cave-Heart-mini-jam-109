extends Resource
class_name CameraShake

export var decay := 0.9
export var max_offset := Vector2(100, 75)
export var max_roll := 0.1
export var noise: OpenSimplexNoise = OpenSimplexNoise.new()
var noise_y := 0.0
var trauma := 0.0
var trauma_power := 2.0
