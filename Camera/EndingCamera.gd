class_name EndingCamera
extends Camera2D

export var ending_position_path: NodePath
onready var ending_position := get_node(ending_position_path)
onready var tween := $Tween


func _ready() -> void:
	pass

func ending() -> void:
	var camera = get_tree().get_nodes_in_group("Camera")[0]
	global_position = camera.global_position
	offset = camera.offset
	zoom = camera.zoom
	self.current = true
	camera.current = false
	tween.interpolate_property(self, "zoom", zoom, Vector2(2, 2), 10.0, Tween.TRANS_EXPO, Tween.EASE_IN_OUT)
	tween.interpolate_property(self, "position", global_position, ending_position.global_position, 10.0, Tween.TRANS_EXPO, Tween.EASE_IN_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	Transition.end_transition()
