extends Camera2D
class_name PlayerCamera

# warning-ignore:unused_signal
signal trigger(msg)

export var debug_mode: bool

export var new_camera_zoom := Vector2(0.5, 0.5)
export var new_camera_offset := Vector2(0, 0)

export var speed = 8

export var min_zoom := 0.5
export var max_zoom := 2.0
export var zoom_factor := 0.1
export var zoom_duration := 0.5

onready var _zoom_level = zoom.x

onready var tween: Tween = Tween.new()
onready var tween_trigger := $TweenSize

export var shake: Resource

func _ready():
	offset = new_camera_offset
	zoom = new_camera_zoom
# warning-ignore:return_value_discarded
	connect("trigger", self, "_on_trigger", [])
	if shake:
		randomize()
		shake.noise.seed = randi()
		shake.noise.period = 4
		shake.noise.octaves = 2
	self.add_child(tween)
	if debug_mode:
		set_physics_process(true)
		set_process_input(true)
		set_process_unhandled_input(true)
	else:
		set_physics_process(false)
		set_process_input(false)
		set_process_unhandled_input(false)

func _process(delta: float) -> void:
	if shake:
		if shake.trauma:
			shake.trauma = max(shake.trauma - shake.decay * delta, 0)
			camera_shake()

func _physics_process(_delta):
	if debug_mode:
		if Input.is_physical_key_pressed(KEY_W):
			move_up()
		if Input.is_physical_key_pressed(KEY_S):
			move_down()
		if Input.is_physical_key_pressed(KEY_D):
			move_right()
		if Input.is_physical_key_pressed(KEY_A):
			move_left()
	
func _unhandled_input(event: InputEvent):
	if event is InputEventMouseButton:
		if event.is_action_pressed("camera_zoom_in", true):
			zoom_in()
		if event.is_action_pressed("camera_zoom_out", true):
			zoom_out()
	
func _set_zoom_level(value: float) -> void:
	# We limit the value between `min_zoom` and `max_zoom`
	_zoom_level = clamp(value, min_zoom, max_zoom)
# warning-ignore:return_value_discarded
	tween.interpolate_property(
		self, 
		"zoom", 
		zoom, 
		Vector2(_zoom_level, _zoom_level), 
		zoom_duration, 
		Tween.TRANS_QUART, 
		Tween.EASE_OUT
	)
# warning-ignore:return_value_discarded
	tween.start()

func move_left() -> void:
	position.x -= speed * _zoom_level

func move_right() -> void:
	position.x += speed * _zoom_level

func move_down() -> void:
	position.y -= -speed * _zoom_level

func move_up() -> void:
	position.y += -speed * _zoom_level

func zoom_in() -> void:
	_set_zoom_level(_zoom_level - zoom_factor)

func zoom_out() -> void:
	_set_zoom_level(_zoom_level + zoom_factor)

func add_trauma(amount: float) -> void:
	shake.trauma = min(shake.trauma + amount, 1.0)

func camera_shake() -> void:
	shake.noise_y += 1
	var amount = pow(shake.trauma, shake.trauma_power)
	rotation = (
		shake.max_roll * 
		amount * 
		shake.noise.get_noise_2d(shake.noise.seed, shake.noise_y)
	)
	offset = Vector2(
		shake.max_offset.x * amount * shake.noise.get_noise_2d(shake.noise.seed*2, shake.noise_y),
		shake.max_offset.y * amount * shake.noise.get_noise_2d(shake.noise.seed*3, shake.noise_y)
	)

func _on_trigger(msg: Dictionary) -> void:
	var event = msg.get("event")
	if event:
		match event:
			"short":
				new_camera_zoom = Vector2(0.5, 0.5)
				new_camera_offset = Vector2(0, 0)
				tween_trigger.interpolate_property(self, "zoom", zoom, Vector2(0.5, 0.5), 2.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
				tween_trigger.interpolate_property(self, "offset", offset, Vector2(0, 100), 2.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
				tween_trigger.start()
			"wide":
				new_camera_zoom = Vector2(1, 1)
				new_camera_offset = Vector2(0, -100)
				tween_trigger.interpolate_property(self, "zoom", zoom, Vector2(1, 1), 3.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
				tween_trigger.interpolate_property(self, "offset", offset, Vector2(0, -100), 3.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
				tween_trigger.start()
	var offset_new = msg.get("offset")
	if offset_new != null:
		new_camera_zoom = zoom
		new_camera_offset = offset_new
		tween_trigger.interpolate_property(self, "offset", offset, offset_new, 2.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
		tween_trigger.start()
