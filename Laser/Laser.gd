tool
class_name Laser
extends RayCast2D

export var turned_on := false
onready var line := $Line2D as Line2D
onready var tween := $Tween

var default_cast = cast_to

var has_turned_on := false

onready var particle := $Line2D/Particles2D
onready var light := $Line2D/Particles2D/Light2D

onready var audio_player := $Line2D/Particles2D/AudioStreamPlayer2D


func _ready() -> void:
	if turned_on:
		audio_player.play()

func _physics_process(_delta: float) -> void:
	if turned_on:
		if is_colliding():
			var collider = get_collider()
			particle.position = to_local(get_collision_point())
			particle.emitting = true
			line.points[1] = to_local(get_collision_point())
			if not has_turned_on:
				tween.interpolate_property(line, "width", line.width, 5.0, .2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
				tween.start()
				has_turned_on = true
			if collider is Player:
				collider.damage()
			if collider is Heart:
				collider.damage()

func open() -> void:
	audio_player.play()
	cast_to = default_cast
	line.points[1] = cast_to
	particle.emitting = true
	tween.interpolate_property(light, "energy", light.energy, 2.0, .1, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(line, "width", line.width, 5.0, .2, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	turned_on = true
	set_physics_process(true)

func close() -> void:
	audio_player.stop()
	cast_to = Vector2(0, 0)
	particle.emitting = false
	tween.interpolate_property(light, "energy", light.energy, 0.0, .1, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.interpolate_property(line, "width", line.width, 0.0, .1, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()
	turned_on = false
	set_physics_process(false)
