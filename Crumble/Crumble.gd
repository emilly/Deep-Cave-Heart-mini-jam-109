extends Node2D

# warning-ignore:unused_signal
signal trigger(msg)

func _ready() -> void:
# warning-ignore:return_value_discarded
	connect("trigger", self, "_on_trigger", [])

func _on_trigger(_msg: Dictionary) -> void:
	$AnimationPlayer.play("crumble")
