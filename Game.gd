extends Node2D

# warning-ignore:unused_signal
signal the_end(msg)

func _ready() -> void:
# warning-ignore:return_value_discarded
	connect("the_end", self, "_on_the_end")
	Transition.transition_out()

func _on_the_end(_msg) -> void:
	$Player.ending()
	$EndingCamera.ending()
