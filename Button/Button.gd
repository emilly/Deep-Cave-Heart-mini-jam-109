class_name IButton
extends Area2D

signal pressed
signal released

export var entity_path: NodePath
export var one_time := false
export var reverse := false
onready var entity = get_node(entity_path)

onready var sprite := $Sprite as AnimatedSprite

var clicked := false

var entities_nearby := []

onready var audio_player := $AudioStreamPlayer2D

func _on_Button_body_entered(body: Node) -> void:
	if not clicked:
		if body.is_in_group("can_press_buttons"):
			audio_player.play()
			sprite.frame = 1
			emit_signal("pressed")
			entities_nearby.append(body)
			if entity:
				if reverse:
					entity.call("close")
				else:
					entity.call("open")
			clicked = true

func _on_Button_body_exited(body: Node) -> void:
	if not one_time:
		if body.is_in_group("can_press_buttons"):
			audio_player.play()
			emit_signal("released")
			var entity_nearby_id = entities_nearby.find(body)
			if entity_nearby_id != -1:
				entities_nearby.remove(entity_nearby_id)
			if entities_nearby.size() == 0 && entity:
				sprite.frame = 0
				clicked = false
				if reverse:
					entity.call("open")
				else:
					entity.call("close")
