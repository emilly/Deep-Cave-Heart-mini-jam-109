extends Node

func load_game() -> void:
	var packed_scene := load("user://saved_scene.tscn")
	if packed_scene:
# warning-ignore:return_value_discarded
		get_tree().change_scene_to(packed_scene)
