extends Node

signal transitioned_into
signal transitioned_out

onready var animation_player = $AnimationPlayer as AnimationPlayer
onready var tween := $Tween as Tween

func transition_into() -> void:
	animation_player.play("transition_into")
	yield(animation_player, "animation_finished")
	emit_signal("transitioned_into")

func transition_out() -> void:
	animation_player.play("transition_out")
	yield(animation_player, "animation_finished")
	emit_signal("transitioned_out")

func end_transition() -> void:
	var volume = AudioServer.get_bus_volume_db(0)
	animation_player.play("transition_into_ending")
	while(volume > -30):
		volume = AudioServer.get_bus_volume_db(0)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), lerp(volume, -72, 0.001))
		yield(get_tree(), "idle_frame")
	$Color.color = Color(0, 0, 0, 255)
	$Ending.color = Color(255, 255, 255, 0)
	get_tree().change_scene("res://Menu/Ending.tscn")
	transition_out()
